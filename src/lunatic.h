/*
 * lunatic.h
 *
 *  Created on: 2018��6��20��
 *      Author: XiaochunTong
 */

#ifndef SPEKA_H_
#define SPEKA_H_


#define REG_MAX 256

#include <vector>
#include <unordered_map>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <memory>
#include <algorithm>
#include <set>
#include <exception>
#include <cassert>
#include <functional>
#include <cmath>

void readFile(const char* filename,std::string&s);
#endif /* SPEKA_H_ */
